package com.example.drawpaint

import android.graphics.RectF

data class MyRectF(
    val rect:RectF,
    val color:Int
)
