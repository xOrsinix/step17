package com.example.drawpaint

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlin.random.Random


class MyPaintView(context:Context,attrs: AttributeSet):View(context,attrs) {

    val BRUSH_SIZE = 20
    val DEFAULT_COLOR = Color.RED
    val DEFAULT_BG_COLOR = Color.WHITE
    val TOUCH_TOLERANCE:Float = 4f
    private var mX:Float = 0f
    private var mY:Float = 0f
    private var mPaint: Paint
    private var circles:ArrayList<MyRectF> = ArrayList()
    private var rects:ArrayList<MyRectF> = ArrayList()
    private var currentColor:Int = 0
    private var backgroundColorMy:Int = DEFAULT_BG_COLOR
    private var strokeWidth:Int = 0
    private var mEmboss:MaskFilter
    private var mBlur: MaskFilter
    private lateinit var mBitmap:Bitmap
    private lateinit var mCanvas: Canvas
    private val mBitmapPaint = Paint(Paint.DITHER_FLAG)


    private var isSquare = true
    private var currentIndex = -1
    private var SIZE = 150
    private var isRandomColor = false

    init {
        mPaint = Paint()
        mPaint.isAntiAlias = true
        mPaint.isDither = true
        mPaint.color = DEFAULT_COLOR
        mPaint.style = Paint.Style.FILL
        mPaint.strokeJoin = Paint.Join.ROUND
        mPaint.strokeCap = Paint.Cap.ROUND
        mPaint.xfermode = null
        mPaint.alpha = 0xff

        mEmboss = EmbossMaskFilter(floatArrayOf(1f, 1f, 1f), 0.4f, 6f, 3.5f)
        mBlur = BlurMaskFilter(5f, BlurMaskFilter.Blur.NORMAL)
    }

    fun init(metrics: DisplayMetrics){
        val height = metrics.heightPixels
        val width = metrics.widthPixels

        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        mCanvas = Canvas(mBitmap)

        currentColor = DEFAULT_COLOR
        strokeWidth = BRUSH_SIZE
    }

    fun square(){
        isSquare=true
    }

    fun circle(){
        isSquare=false
    }


    fun size_normal() {
        SIZE=100
    }

    fun size_big() {
        SIZE=150
    }

    fun size_small() {
        SIZE=50
    }

    fun color_green() {
        isRandomColor = false
        currentColor = Color.GREEN
    }

    fun color_red() {
        isRandomColor = false
        currentColor = Color.RED
    }

    fun color_black() {
        isRandomColor = false
        currentColor = Color.BLACK
    }

    fun color_random(){
        isRandomColor = true
        currentColor = Color.argb(255, Random.nextInt(256),Random.nextInt(256),Random.nextInt(256))
    }


    fun clear() {
        backgroundColorMy = DEFAULT_BG_COLOR
        rects.clear()
        circles.clear()
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        canvas.save()
        mCanvas.drawColor(backgroundColorMy)
        for (rect in rects) {
            mPaint.color = rect.color
            mPaint.strokeWidth = strokeWidth.toFloat()
            mPaint.maskFilter = null
            mCanvas.drawRect(rect.rect, mPaint)
        }
        for (circle in circles){
            mPaint.color = circle.color
            mPaint.strokeWidth = strokeWidth.toFloat()
            mPaint.maskFilter = null
            mCanvas.drawOval(circle.rect,mPaint)
        }
        canvas.drawBitmap(mBitmap, 0f, 0f, mBitmapPaint)
        canvas.restore()
    }

    private fun touchStartSquare(x: Float, y: Float) {
        var flag = true
        for (i in rects.indices)
            if (rects[i].rect.left <= x && rects[i].rect.right >= x && rects[i].rect.top >= y && rects[i].rect.bottom <= y)
                flag = false
        if (flag) {
            val rect = RectF(x - SIZE, y + SIZE, x + SIZE, y - SIZE)
            if (isRandomColor)
                color_random()
            rects.add(MyRectF(rect,currentColor))
            currentIndex = rects.size-1
        }
        mX = x
        mY = y
    }

    private fun touchStartCircle(x: Float, y: Float) {
        var flag = true
        for (i in circles.indices) {
            val cy = (circles[i].rect.top + circles[i].rect.bottom)/2
            val cx = (circles[i].rect.left+circles[i].rect.right)/2
            val r = circles[i].rect.top-cy
            if (Math.sqrt(Math.pow((cx-x).toDouble(),(2).toDouble())+Math.pow((cy-y).toDouble(),(2).toDouble()))<=r)
                flag = false
        }
        if (flag) {
            val rect = RectF(x - SIZE, y + SIZE, x + SIZE, y - SIZE)
            if (isRandomColor)
                color_random()
            circles.add(MyRectF(rect,currentColor))
            currentIndex = circles.size-1
        }
        mX = x
        mY = y
    }

    private fun touchMoveSquare(x: Float, y: Float) {
        val dx = Math.abs(x - mX)
        val realDx = x-mX
        val dy = Math.abs(y - mY)
        val realDy = y-mY
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            if(currentIndex==-1) {
                for (i in rects.indices) {
                    if (rects[i].rect.left <= x && rects[i].rect.right >= x && rects[i].rect.top >= y && rects[i].rect.bottom <= y) {
                        currentIndex = i
                        break
                    }
                }
            }
            if (currentIndex!=-1){
                rects[currentIndex].rect.offset(realDx,realDy)
            }
            mX = x
            mY = y
        }
    }

    private fun touchMoveCircle(x: Float, y: Float) {
        val dx = Math.abs(x - mX)
        val realDx = x-mX
        val dy = Math.abs(y - mY)
        val realDy = y-mY
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            if(currentIndex==-1) {
                for (i in circles.indices) {
                    val cy = (circles[i].rect.top + circles[i].rect.bottom)/2
                    val cx = (circles[i].rect.left+circles[i].rect.right)/2
                    val r = circles[i].rect.top-cy
                    if (Math.sqrt(Math.pow((cx-x).toDouble(),(2).toDouble())+Math.pow((cy-y).toDouble(),(2).toDouble()))<=r){
                        currentIndex = i
                        break
                    }
                }
            }
            if (currentIndex!=-1){
                circles[currentIndex].rect.offset(realDx,realDy)
            }
            mX = x
            mY = y
        }
    }

    private fun touchUp() {
        currentIndex=-1
    }

    private fun touchStart(x: Float, y: Float){
        if (isSquare)
            return touchStartSquare(x,y)
        else
            return touchStartCircle(x,y)
    }

    private fun touchMove(x: Float, y: Float){
        if (isSquare)
            return touchMoveSquare(x,y)
        else
            return touchMoveCircle(x,y)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                touchStart(x, y)
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                touchMove(x, y)
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                touchUp()
                invalidate()
            }
        }
        return true
    }

}