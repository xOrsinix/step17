package com.example.drawpaint

import android.hardware.display.DisplayManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Menu
import android.view.MenuItem


class MainActivity : AppCompatActivity() {

    lateinit var paintView: MyPaintView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        paintView = findViewById(R.id.paintView)
        val displayMetrics:DisplayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        paintView.init(metrics = displayMetrics)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.my_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.clear -> paintView.clear()
            R.id.size_small -> paintView.size_small()
            R.id.size_normal -> paintView.size_normal()
            R.id.size_big -> paintView.size_big()
            R.id.color_green -> paintView.color_green()
            R.id.color_red -> paintView.color_red()
            R.id.color_black -> paintView.color_black()
            R.id.color_random -> paintView.color_random()
            R.id.square -> paintView.square()
            R.id.circle -> paintView.circle()
        }
        return true
    }
}